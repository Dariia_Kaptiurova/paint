package sample;

import com.sun.jdi.connect.Connector;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.Arguments;

import java.io.File;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;
import javafx.stage.Stage;

class MainTest {

//    @Test
//    void start() {
//        Main start = mock(Main.class);
//        doNothing().when(start).show(isA(Stage.class));
//        start.show(null);
//
//        verify(start, times(1)).show(null);
//    }


    @Test
    void main() {
        Main main = mock(Main.class);
        doNothing().when(main).launch(isA(String.class));
        main.launch(null);

        verify(main, times(1)).launch(null);
    }
}