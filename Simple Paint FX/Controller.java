package sample;

import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.canvas.Canvas;

import javax.imageio.ImageIO;

import javafx.beans.value.ChangeListener;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;


import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Controller {

    @FXML
    private Canvas canvas;

    @FXML
    private Slider brushSize;

    @FXML
    private ColorPicker colorPicker;

    @FXML
    private Button save;

    @FXML
    private CheckBox erase;

    @FXML
    private Label value;

    @FXML
    private Button open;

    @FXML
    private Button delete;


    public void initialize() {

        GraphicsContext graphic = canvas.getGraphicsContext2D();

        canvas.setOnMouseDragged(e -> {
            double size = brushSize.getValue();
            double x = e.getX() - size / 2;
            double y = e.getY() - size / 2;

            if (erase.isSelected()) {
                graphic.clearRect(x, y, size, size);
            } else {
                graphic.setFill(colorPicker.getValue());
                graphic.fillRect(x, y, size, size);
            }
        });


        brushSize.valueProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                value.setText(String.format("%.0f", newValue));
                graphic.setLineWidth(brushSize.getValue());
            }
        });
    }

    public void save() {

        save.setOnAction((e) -> {
            FileChooser saveFile = new FileChooser();
            saveFile.setTitle("Save File");
            saveFile.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("PNG files(*.png)", "*.png"),
                    new FileChooser.ExtensionFilter("PNG files(*.jpg)", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG files(*.bmp)", "*.bmp"),
                    new FileChooser.ExtensionFilter("PNG files(*.gif)", "*.gif"));

            File file = saveFile.showSaveDialog(null);
            if (file != null) {
                try {
                    WritableImage writableImage = new WritableImage(1080, 790);
                    canvas.snapshot(null, writableImage);
                    RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
                    ImageIO.write(renderedImage, "png", file);
                } catch (IOException ex) {
                    System.out.println("Error!");
                }
            }
        });
    }

    public void deleteAll(ActionEvent actionEvent) {
        delete.setOnAction((e) -> {
            GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
            graphicsContext.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        });
    }

    @FXML
    private void open() {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        FileChooser openFile = new FileChooser();
        openFile.setTitle("Open File");
        File file = openFile.showOpenDialog(null);
        if (file != null) {
            try {
                InputStream io = new FileInputStream(file);
                Image img = new Image(io);
                graphicsContext.drawImage(img, 0, 0);
            } catch (IOException ex) {
                System.out.println("Error!");
            }
        }
    }
}