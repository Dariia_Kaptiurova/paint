package com.example.androidsimplepaint;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.MaskFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.jar.Attributes;

public class UI extends View {

    public static int BRUSH_SIZE = 10;
    public static final int DEFAULT_COLOR = Color.RED;
    public static final int DEFAULT_BG_COLOR = Color.WHITE;
    private static final float TOUCH_TOLERANCE = 4;
    private float mX, mY;
    private Path myPath;
    private Paint myPaint;
    private ArrayList<FingerLine> path = new ArrayList<>();
    private int currentColor;
    private int backgroundColor = DEFAULT_BG_COLOR;
    private int strokeWidths;
    private EmbossMaskFilter myEmboss;
    private BlurMaskFilter myBlur;
    private Bitmap myBitmap;
    private Canvas myCanvas;
    private Paint myBitmapPaint = new Paint(Paint.DITHER_FLAG);
    private boolean emboss;
    private boolean blur;


    public UI(Context context) {
        super(context);
    }

    public UI(Context context, Attributes attrs) {
        super(context, (AttributeSet) attrs);
        myPaint = new Paint();
        myPaint.setAntiAlias(true);
        myPaint.setDither(true);
        myPaint.setColor(DEFAULT_COLOR);
        myPaint.setStyle(Paint.Style.STROKE);
        myPaint.setStrokeJoin(Paint.Join.ROUND);
        myPaint.setStrokeCap(Paint.Cap.ROUND);
        myPaint.setXfermode(null);
        myPaint.setAlpha(0xff);

        myEmboss = new EmbossMaskFilter(new float[]{1, 1, 1}, 0.4f, 6, 3.5f);
        myBlur = new BlurMaskFilter(5, BlurMaskFilter.Blur.NORMAL);
    }

    public void init(DisplayMetrics metrics) {
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;

        myBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        myCanvas = new Canvas(myBitmap);

        currentColor = DEFAULT_COLOR;
        strokeWidths = BRUSH_SIZE;
    }

    public void normal() {
        emboss = false;
        blur = false;
    }

    public void emboss() {
        emboss = true;
        blur = false;
    }

    public void blur() {
        emboss = false;
        blur = true;
    }

    public void clear() {
        backgroundColor = DEFAULT_BG_COLOR;
        path.clear();
        normal();
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.save();
        myCanvas.drawColor(backgroundColor);

        for (FingerLine fl : path) {
            myPaint.setColor(fl.color);
            myPaint.setStrokeWidth(fl.lineWidths);
            myPaint.setMaskFilter(null);

            if (fl.emboss)
                myPaint.setMaskFilter(myEmboss);
            else if (fl.blur)
                myPaint.setMaskFilter(myBlur);

            myCanvas.drawPath(fl.path, myPaint);
        }

        canvas.drawBitmap(myBitmap, 0, 0, myBitmapPaint);
        canvas.restore();
    }

    private void touchStart(float x, float y) {
        myPath = new Path();
        FingerLine fl = new FingerLine(currentColor, emboss, blur, strokeWidths, myPath);
        path.add(fl);

        myPath.reset();
        myPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private void touchMove(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);

        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            myPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
    }

    private void TouchUp() {
        myPath.lineTo(mX, mY);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchStart(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touchMove(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                invalidate();
                break;
        }
        return true;
    }
}
