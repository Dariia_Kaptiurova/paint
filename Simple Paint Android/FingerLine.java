package com.example.androidsimplepaint;

import android.graphics.Path;

public class FingerLine {

    public int color;
    public boolean emboss;
    public boolean blur;
    public int lineWidths;
    public Path path;

    public FingerLine (int color, boolean emboss, boolean blur, int lineWidths, Path path) {
        this.color = color;
        this.emboss = emboss;
        this.blur = blur;
        this.lineWidths = lineWidths;
        this.path = path;
    }
}
